#!/usr/bin/python3

"""Cartesian Line Tests"""

import unittest
import pycodestyle

from converter import calculate, main


class CartesianTests(unittest.TestCase):
    def test_calculation(self):
        """Ensure that points are correctly converted into standard form."""
        def check(input, output):
            """Shorthand for frequently-called assertion."""
            self.assertEqual(calculate(*input), output)

        # Simplest cases, A = B = 1
        check(('0,0', '1,-1'), 'x + y = 0')
        check(('1,-1', '0,0'), 'x + y = 0')
        check(('0,1', '1,0'), 'x + y = 1')
        check(('1,1', '2,0'), 'x + y = 2')

        # Try some different values for A, B, and C
        check(('0,2', '3,0'), '2x + 3y = 6')
        check(('-3,2', '3,-2'), '2x + 3y = 0')
        check(('-1,-1', '-4,1'), '2x + 3y = -5')

        # Subtraction for upward sloping lines
        check(('0,0', '6,10'), '5x - 3y = 0')
        check(('2,3', '5,8'), '5x - 3y = 1')
        check(('4,7', '-2,-3'), '5x - 3y = -1')

        # Extreme angles
        check(('0.8,0', '1,20000'), '100000x - y = 80000')
        check(('0.999,0', '1,20000'), '20000000x - y = 19980000')
        check(('0,0.1', '1000000,1'), 'x - 1000000y = -100000')

        # Fractional values
        check(('1.5,0', '0,1.5'), 'x + y = 1.5')
        check(('-0.5,1', '1.5,0'), 'x + 2y = 1.5')

        # The dreaded corner cases involving zero!
        check(('1,0', '2,0'), '0x + y = 0')
        check(('1,7', '2,7'), '0x + y = 7')
        check(('0,1', '0,2'), 'x + 0y = 0')
        check(('5,1', '5,2'), 'x + 0y = 5')
        check(('1,1', '1,1'), 'Undefined: Only one unique point given.')
        check(('gar', 'bage'), 'Undefined: Could not understand input.')
        check(('1,2,3', '0,0'), 'Undefined: Could not understand input.')

    def test_main(self):
        """Ensure commandline args are processed."""
        self.assertEqual(main('test'), 1)
        self.assertEqual(main('test', '0,0'), 1)
        self.assertEqual(main('test', '0,0', '1,1'), 0)

    def test_code_style(self):
        """Ensure that all code adheres to PEP8 standards."""
        style = pycodestyle.StyleGuide()
        result = style.check_files(['converter.py', 'tests.py'])
        self.assertEqual(
            result.total_errors, 0,
            'Code does not adhere to PEP8 rules.')


if __name__ == '__main__':
    unittest.main(buffer=True)
