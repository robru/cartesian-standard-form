#!/usr/bin/python3

"""Cartesian Line Tool

Usage: {} x1,y1 x2,y2

You must supply an even number of coordinates. The coordinates will be
paired and lines calculated from the pairs. For example:

    $ ./converter.py 0,0 1,2 1,1 2,2
    2x - y = 0
    x - y = 0

The equation "2x - y = 0" describes the line that intersects 0,0 1,2 while
"x - y = 0" describes the line that intersects 1,1 2,2.
"""


import re
from fractions import Fraction

POINT = re.compile(r'(-?\d+\.?\d*)')


def parse_point(raw):
    """Convert strings like "(x,y)" into floats."""
    return [float(flt) for flt in POINT.findall(raw)]


def no_point_zero(raw):
    """Truncate any stray '.0' from the end of a string."""
    return raw[:-2] if raw.endswith('.0') else raw


def pretty(number):
    """Convert a coefficient to a string, omitting if it's 1."""
    return str(number) if number != 1 else ''


def calculate(first, last):
    """Return string in the form of 'Ax + By = C'."""
    # Extract floats from string inputs
    try:
        x1, y1 = parse_point(first)
        x2, y2 = parse_point(last)
    except ValueError:
        return 'Undefined: Could not understand input.'

    if x1 == x2:
        if y1 == y2:
            return 'Undefined: Only one unique point given.'
        # Vertical line, avoid division by zero.
        return no_point_zero('x + 0y = {}'.format(x1))

    # Calculate the slope
    deltax = x2 - x1
    deltay = y2 - y1
    slope_float = deltay / deltax
    slope = Fraction.from_float(abs(slope_float)).limit_denominator(1000000)
    add = slope_float <= 0

    # Calculate the final offset C
    Ca = slope.numerator * x1
    Cb = slope.denominator * y1
    C = Ca + Cb if add else Ca - Cb

    # Format the string representation of the equation
    return no_point_zero('{}x {} {}y = {}'.format(
        pretty(slope.numerator),
        '+' if add else '-',
        pretty(slope.denominator),
        C))


def main(appname, *args):
    """Capture input and print output."""
    nargs = len(args)
    if nargs % 2 != 0 or nargs < 2:
        print(__doc__.format(appname))
        return 1

    # Consume the supplied arguments in pairs
    args = iter(args)
    for arg in args:
        print(calculate(arg, next(args)))
    return 0


if __name__ == '__main__':  # pragma: nocover
    import sys
    sys.exit(main(*sys.argv))
