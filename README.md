Cartesian Line Calculator
=========================

This is a simple tool that takes two points on a cartesian plane and calculates
the line that intersects them, reported in the standard form:

    Ax + By = C


Usage
-----

To run the script, pass pairs of cartesian points as commandline args:

    $ ./converter.py 0,0 1,1
    x - y = 0


Testing
-------

To run the tests you first need to install the dependencies:

    $ pip3 install pycodestyle coverage


Then run the unit tests with the coverage tool:

    $ coverage run ./tests.py
    ...
    ----------------------------------------------------------------------
    Ran 3 tests in 1.133s

    OK
    $ coverage report --show-missing --fail-under=100
    Name           Stmts   Miss  Cover   Missing
    --------------------------------------------
    converter.py      39      0   100%
    tests.py          19      0   100%
    --------------------------------------------
    TOTAL             58      0   100%

